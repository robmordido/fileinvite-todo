<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TodoRequest;
use App\Todo;
use App\TodoLog;

class TodoController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::orderBy('id','desc')->get();        
        return view('todo.index',compact('todos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoRequest $request)
    {
        $todo = new Todo;
        $todo->title = $request->get('title');
        $todo->save();
        $this->writeLog("Added new todo #{$todo->id} - $todo->title");
        return $todo;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($todo)
    {
        return $todo;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TodoRequest $request, $todo)
    {
        $todo->title    = ($request->get('title') !== null) ? $request->get('title') : $todo->title;
        $todo->done     = ($request->get('done')) ?: 0;
        $todo->save();
        $event = ($todo->done) ? "Done" : "Undone";
        $this->writeLog("$event todo #{$todo->id} - $todo->title");
        return $todo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($todo)
    {
        $this->writeLog("Removed todo #{$todo->id} - $todo->title");
        return response()->json(['success'=>$todo->delete()]);
    }

    /**
     * Display statistics for todo.
     *
     * @return \Illuminate\Http\Response
     */
    public function counters(){
        return response()->json([
            'total_done' => Todo::whereDone(1)->count(),
            'total_todo' => Todo::count()
        ]);
    }

    /**
     * Display activity logs.
     *
     * @return \Illuminate\Http\Response
     */
    public function logs(){
       return TodoLog::orderBy('id','desc')->limit(10)->get();
    }

    /**
     * Creates log entry
     *
     * @param  string  $text
     * @return \Illuminate\Http\Response
     */
    public function writeLog($text){
        $log = new TodoLog();
        $log->text = $text;
        return $log->save();
    }
}
