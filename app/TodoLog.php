<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoLog extends Model
{
    protected $table = 'logs';
}
