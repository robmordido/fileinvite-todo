var todoApp = function(){
	var self 			= this;
	this.todoList 		= $('#todoList');
	this.todoModal 		= $('#todoModal');
	this.todoModalForm 	= $('#todoModalForm');
	this.csrfToken 		= $('meta[name="csrf-token"]').attr('content');

	this.init = function(){
		self.listActions();
		self.modalActions();
		self.updateActivity();
	}
	this.updateTodo = function(id,done){
		var todo = $('#todo-item-'+id);
		self.request('put','/todo/'+id,{done : done})
		.done(function(result){
			if(result.done){
				todo.addClass('done');
				todo.find('input[type="checkbox"]').attr('checked',true);
			}else{
				todo.removeClass('done');
				todo.find('input[type="checkbox"]').attr('checked',false);
			}
			self.updateActivity();
		});
	}
	this.deleteTodo = function(id){
		var todo = $('#todo-item-'+id);
		self.request('delete','/todo/'+id)
		.done(function(){
			todo.remove();
			if(self.todoList.find('li').length == 0){
				self.todoList.append('<p>You have nothing todo :)</p>')
			}
			self.updateActivity();
		});
	}
	this.request = function(method,url,params){
		var params = params || {};
		return $.ajax({
			url : url,
			method : method,
			data : JSON.stringify(params),
			dataType: 'json',
			headers : {				
				'X-CSRF-TOKEN' : self.csrfToken,
				'Content-Type' : 'application/json'
			}
		});
	}
	this.modalActions = function(){
		self.todoModalForm.unbind().submit(function(e){
			self.request('post','/todo',{title : self.todoModalForm.find('#title').val()})
			.done(function(todo){
				var done = (todo.done) ? 'done' : '';
				var html = '';
				html += '<li id="todo-item-'+todo.id+'" data-todoid="'+todo.id+'" class="list-group-item todo-item '+done+'"><a href="javascript:;"><input class="todo-item-checkbox" type="checkbox"> '+todo.title+'</a>';
				html += '<span class="float-right">';
				html += '<button class="btn btn-danger btn-sm todo-delete"><i class="fa fa-trash"></i> Delete</button>';
				html += '</span>';
				html += '</li>';

				if(self.todoList.find('li').length == 0){self.todoList.html('')}

				self.todoList.prepend(html);
				self.todoModal.modal('hide');
				self.listActions();
				self.updateActivity();
			});
			e.preventDefault();
		});

		self.todoModal.on('show.bs.modal', function (e) {
			self.todoModal.find('#title').val('');
		});
		self.todoModal.on('shown.bs.modal', function (e) {
			self.todoModal.find('#title').focus();
		});
	}
	this.listActions = function(){
		$('.todo-item a').unbind().click(function(){
			var todoId 	= $(this).parent().data('todoid');
			var done 	= $(this).parent().hasClass('done') ? false : true;
			self.updateTodo(todoId,done);
		});

		$('.todo-delete').unbind().click(function(){
			var todoId 	= $(this).parent().parent().data('todoid');
			self.deleteTodo(todoId);
		});
	}
	this.updateActivity = function(){
		self.request('get','/todo/counters')
		.done(function(result){
			$('#total_done').text(result.total_done);
			$('#total_todo').text(result.total_todo);
		});

		self.request('get','/todo/logs')
		.done(function(logs){
			$('#activityLogs').html('');
			$.each(logs,function(i,log){
				$('#activityLogs').append('<li class="list-group-item">'+log.text+'</li>');
			});
		});
	}
}
$(document).ready(function(){
	todo = new todoApp();
	todo.init();	
});
