@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                	Todo List (<span id="total_done">--</span>/<span id="total_todo">--</span>)
	                <div class="float-right">
	                	<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#todoModal">
						  <i class="fa fa-plus"></i> Add Todo
						</button>
	                </div>
	            </div>

                <div class="card-body">
					<ul id="todoList" class="list-group list-group-flush">
						@if($todos->count() > 0)
						@foreach($todos as $todo)
							<li id="todo-item-{{$todo->id}}" data-todoid="{{$todo->id}}" class="list-group-item todo-item {{($todo->done) ? 'done' : ''}}">
								<a href="javascript:;"><input class="todo-item-checkbox" type="checkbox" {{($todo->done) ? "checked" : ""}}> {{$todo->title}}</a>
								<span class="float-right">
									<button class="btn btn-danger btn-sm todo-delete"><i class="fa fa-trash"></i> Delete</button>
								</span>
							</li>
						@endforeach
						@else
							<p>You have nothing todo :)</p>
						@endif
					</ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                	Activity Logs
	            </div>

                <div class="card-body">
					<ul id="activityLogs" class="list-group list-group-flush">
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="todoModal" tabindex="-1" role="dialog" aria-labelledby="todoModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  	<form id="todoModalForm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="todoModalLabel">Todo List</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        	<input id="title" name="title" class="form-control" placeholder="What to do?" required>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection
