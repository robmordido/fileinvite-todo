<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
	return redirect()->route('todo.index');
});
Route::get('/todo/counters','TodoController@counters');
Route::get('/todo/logs','TodoController@logs');
Route::resource('todo','TodoController')->except(['create','edit']);